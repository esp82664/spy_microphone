<h1>Spy Microphone</h1>
<p>
In this project I demonstrate how to use an NodeMCU and an electret microphone to transmit data via UDP, basically the voltage that comes from the microphone is read and converted into PCM format and sent in intervals of 125us, which corresponds to an audio output of 8kHz (quality from phone).
To receive and decode the signal I use "netcat + aplay".
<p>
<h4>Command Line</h4>
<p>
nc -lvup 5001 | aplay -r 8000 -f U8
