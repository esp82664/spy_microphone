#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <ESP8266WebServer.h>

WiFiUDP udp;
const int port = 5001; //any UDP port
// IPAddress broadcast;
IPAddress broadcast = IPAddress(192, 168, 0, XXX);
const char* ssid = "XXXXXXXX";
const char* password = "XXXXXXXX";
int analogPin = A0;
int voltage = 0;
int pcm = 0;

void setup()
{ 
  pinMode(analogPin, INPUT);  
  Serial.begin(115200);
  Serial.println("Startup");
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("broadcast address : ");
  Serial.print(broadcast);
  Serial.print(":");
  Serial.println(port);
}

void loop()
{
  udp.beginPacketMulticast(broadcast, port, WiFi.localIP());
  for(int i=0; i < 1023; i++)
  {
    int old = micros();
    voltage = analogRead(analogPin);
    pcm = map(voltage, 0, 1023, 0, 255);   
    udp.write(pcm);
    while(micros() - old < 122); // 125us = 1s/8000Hz - 3us to other proccess
  }
  udp.endPacket();
  delay(5);
}
